from django.db import models
from datetime import datetime, date

# Create your models here.
class Aktivitas(models.Model):
    date = models.DateTimeField()
    activity = models.CharField(max_length=30)
    location = models.CharField(max_length=30)
    category = models.CharField(max_length=30)