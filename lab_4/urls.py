from django.urls import path
from .views import *

urlpatterns = [
 url('HOME', views.home, name = 'home'),
 url('EDUCATION/', views.education, name = 'education'),
 url('PROFILE/', views.profile, name = 'profile'),
 url('REGISTER/', views.regist, name = 'regist'),
 url('saveact/',views.saveact, name = 'saveact'),
 url('fillact/',views.fillact, name = 'fillact'),
 url('showact/',views.showact, name = 'showact'),
 url('delete/', views.delete, name = 'delete')
]
