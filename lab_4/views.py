from django.shortcuts import render
from django.http import HttpResponse
from .models import Aktivitas
from .forms import Message_Form


def home(request):
    return render(request, 'HOME.html')

def education(request):
    return render(request, 'EDUCATION.html')

def profile(request):
    return render(request, 'PROFILE.html')

def regist(request):
    return render(request, 'REGISTER.html')
response = {}
def saveact(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST'):
        response['date'] = request.POST['date']
        response['activity'] = request.POST['activity']
        response['location'] = request.POST['location']
        response['category'] = request.POST['category']
        Aktivitas.objects.create(date=response['date'],activity=response['activity'],location=response['location'],category=response['category'])
        # jadwal.save()
        html = 'responseforms.html'
        return render(request,html,response)
    else:
        return HttpResponse('responseforms.html')
def fillact(request):
    response['saveact'] = Message_Form
    return render(request, 'responseforms.html', response)
def showact(request):
    jadwal = Aktivitas.objects.all().values()
    response['jadwal'] = jadwal
    return render(request, 'result.html',response)
def delete(request):
    jadwal = Aktivitas.objects.all().delete()
    response['jadwal'] = jadwal
    return render(request, 'result.html', response)

