"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.urls import re_path
from django.contrib import admin
from lab_4.views import home
from lab_4.views import education
from lab_4.views import profile
from lab_4.views import regist
from lab_4.views import showact
from lab_4.views import saveact
from lab_4.views import fillact
from lab_4.views import delete


urlpatterns = [
    url(r'^admin', admin.site.urls),
    url(r'^$', home, name="home"),
    url(r'^education', education, name="education"),
    url(r'^profile', profile, name="profile"),
    url(r'^regist', regist, name="regist"),
    url(r'^saveact',saveact, name = 'saveact'),
    url(r'^fillact',fillact, name = 'fillact'),
    url(r'^showact',showact, name = 'showact'),
    url(r'^delete',delete, name = 'delete'),

] #+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
